import { Route } from "react-router-dom";
import HeaderComponent  from "./components/HeaderComponent/HeaderComponent";
import EmployeeComponent from "./components/EmployeeComponent/EmployeeComponent";
import ApiComponent from "./components/ApiComponent/ApiComponent";

const App = () => {
  return (
  
    <header>
      <HeaderComponent/>
      <Route path='/Component'><EmployeeComponent/></Route>
        <Route path='/ApiCall'><ApiComponent/></Route>
      </header>
      
  );
};

export default App;