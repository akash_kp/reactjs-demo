import { NavLink } from 'react-router-dom';
import classes from './HeaderComponent.module.css';

const HeaderComponent = () =>{
    return (
        <header className={classes.header}>
          <nav>
            <ul>
              <li>
                <NavLink activeClassName={classes.active} to='/Component'>
                Component
                </NavLink>
              </li>
              <li>
                <NavLink activeClassName={classes.active} to='/ApiCall'>
                ApiCall
                </NavLink>
              </li>
            </ul>
          </nav>
        </header>
      );
}

export default HeaderComponent