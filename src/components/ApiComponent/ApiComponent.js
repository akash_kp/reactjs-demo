import React, { useState } from "react";
import "./ApiComponent.css"

const ApiComponent = (props) => {
  const [responceData, setResponceData] = useState([]);
  const [isApiCalled, setApiStatus] = useState(false)

  function fetchApiData() {
    setApiStatus(true)
    fetch("https://api.coinbase.com/v2/currencies")
      .then((responce) => {
        return responce.json();
      })
      .then((data) => {
        setResponceData(data.data);
      });
  }

  function clearTableData(){
    setApiStatus(false)
  }

  let dispApiData ;

  if(isApiCalled){
    dispApiData = responceData.map((tempApiData) => (
      <tr key={tempApiData.id}>
        <td>{tempApiData.id}</td>
        <td>{tempApiData.name}</td>
      </tr>
  ));
  }else{
    dispApiData = <p></p>;
  }

  return (
    <div>
      <button class="b1" onClick={fetchApiData}> Call API</button>
      <button class="b2" onClick={clearTableData}> Clear Data</button>
      <table id="currecy">
      {dispApiData}
      </table>
    </div>
  );
};

export default ApiComponent;
